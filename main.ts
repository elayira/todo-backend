import rdb from "./src/dataAccess/connections"
import { logger } from "./src/logger"
import { graphqlApp } from "./src/servers"
import { restApp } from "./src/servers"
import { config } from "./src/settings"

const servers: Record<"graph" | "rest", (port?:number) => void | Promise<void>> = {
  graph: async (port=config.PORT) => {
    const app = await graphqlApp()
    await app.listen(port)
    logger.info(`READY!: GraphQL server`)
    logger.info(`Now running at http://${config.HOST}:${port}/graphql/`)
  },
  rest: (port=config.PORT) => {
    restApp().listen(port, config.HOST, () => {
      logger.info(`READY!: REST server`)
      logger.info(`Now running at http://${config.HOST}:${port}/api/v1/`)
    })
  }
}

rdb
  .initialize()
  .then(() => {logger.info("Data store initialized!")})
  .catch((err) => {logger.error("Error initializing data store:", err)})

if (servers[config.APP_BACKEND]) {
  servers[config.APP_BACKEND]()
} else {
  servers.graph()
  servers.rest(config.PORT + 1)
}

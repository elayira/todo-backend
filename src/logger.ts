import pino from 'pino'
import { config } from './settings'


export const logger = pino({
  transport: {target: 'pino-pretty'},
  level: config.LOG_LEVEL,
  timestamp: pino.stdTimeFunctions.isoTime,
})

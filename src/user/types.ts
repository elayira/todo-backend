export interface IUser {
  readonly id: string;
  readonly createdAt: Date;
  lastName: string;
  otherNames: string;
  email: string;
}
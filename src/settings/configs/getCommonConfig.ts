import * as path from "path"
import { DefaultSettings } from "../config.type"

export function getCommonConfig(processVariables: NodeJS.ProcessEnv): DefaultSettings {
  const ROOT_DIR = path.join(__dirname, '..', '..', '..')
  const ASSETS_DIR = path.join(ROOT_DIR, 'assets')
  const OPEN_API = {
    API_SPEC: path.join(ASSETS_DIR, 'openapi.json'),
    VALIDATE_REQUESTS: true,
    VALIDATE_RESPONSES: true,
  }
  const DEBUG = processVariables.NODE_ENV?? 'production'? true : false
  const LOG_LEVEL = processVariables.LOG_LEVEL ?? ( DEBUG? 'debug': 'info' )
  
  return {
    DEBUG,
    ROOT_DIR,
    ASSETS_DIR,
    OPEN_API,
    REST_API_ROOT: processVariables.REST_API_ROOT ?? 'api/',
    GRAPHQL_ROOT: processVariables.GRAPHQL_ROOT ?? 'graphql/',
    LOG_LEVEL,
    PORT: parseInt(processVariables.PORT ?? `8080`),
    HOST: processVariables.HOST ?? '0.0.0.0',
    DATABASE: {},
    NODE_ENV: 'development',
    APP_BACKEND: processVariables.APP_BACKEND,
    AUTHENTICATION: {
      enabled: true,
      jwksUrl: "",
    }
  }
}
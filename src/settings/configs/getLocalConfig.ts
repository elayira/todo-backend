import { DefaultSettings } from "../config.type";
import { getCommonConfig } from "./getCommonConfig";

export function getLocalConfig(processVariables: NodeJS.ProcessEnv): DefaultSettings {
  
  return {
    ...getCommonConfig(processVariables),
    DATABASE: {
      url: processVariables.DATABASE_URL,
      ssl: false,
    },
    NODE_ENV: processVariables.NODE_ENV ?? 'development',
    AUTHENTICATION: {
      enabled: false,
      jwksUrl: "",
    },
  };
}

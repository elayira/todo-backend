import { DefaultSettings } from "../config.type";
import { getLocalConfig } from "./getLocalConfig"

export function getConfig(processVariables: NodeJS.ProcessEnv): DefaultSettings {
  const environment: NodeJS.ProcessEnv['NODE_ENV'] = processVariables.NODE_ENV ?? "development";
  switch (environment) {
    default:
      return getLocalConfig(processVariables);
  }
}

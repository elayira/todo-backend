interface DatabaseConnectionConfig {
  connection?: {
    user?: string;
    database?: string;
    password?: string | (() => string | Promise<string>);
    port?: number;
    host?: string;
    connection?: Record<string, any>;
    keep_alive?: boolean;
  }
  url?: string;
  ssl?: boolean;
}

interface Authentication {
  enabled: boolean;
  jwksUrl: string;
}

export interface OpenAPIConfig {
  API_SPEC: string;
  VALIDATE_REQUESTS: boolean;
  VALIDATE_RESPONSES: boolean;
}

export interface DefaultSettings {
  DEBUG: boolean;
  ROOT_DIR: string;
  ASSETS_DIR: string;
  OPEN_API: OpenAPIConfig;
  REST_API_ROOT: string;
  GRAPHQL_ROOT: string;
  LOG_LEVEL: NodeJS.ProcessEnv['LOG_LEVEL'];
  PORT: number;
  HOST: string;
  DATABASE: DatabaseConnectionConfig;
  NODE_ENV: NodeJS.ProcessEnv['NODE_ENV'];
  APP_BACKEND: NodeJS.ProcessEnv['APP_BACKEND']
  AUTHENTICATION: Authentication;
}

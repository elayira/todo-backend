import { getConfig } from "./configs/getConfig";
import * as dotenv from 'dotenv';
dotenv.config()

export const config = getConfig(process.env);
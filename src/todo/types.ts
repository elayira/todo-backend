import { IUser } from "../user/types";

export type Id = string;
export enum TodoStatus {
  COMPLETED = 3,
  ARCHIVED = 4,
  ONGOING = 2,
  ASSIGNED = 1
}

export enum TaskStatus {
  COMPLETED = 3,
  UNASSIGNED = 0,
  ONGOING = 2,
  ASSIGNED = 1
}

export interface ITask {
  readonly id: Id;
  readonly todoId: Id;
  status: TaskStatus;
  readonly createdAt: Date;
  description: string;
  dueDate?: Date;
}

export interface ITodo {
  readonly id: Id;
  readonly createdAt: Date;
  author?: IUser | null;
  tasks?: ITask[];
  dueDate?: Date | null;
  status: TodoStatus;
  name: string;
  description: string;
}

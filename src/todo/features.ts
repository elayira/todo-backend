import { ITodo } from "./types";

import Todo from "../dataAccess/todo.dao";

export async function createTodo(name: string, description: string): Promise<ITodo> {
  return Todo.create({name, description});
}

export async function retrieveAllTodo(): Promise<ITodo[]> {
  const todos = new Todo().all();
  return todos;
}

export async function retrieveTodo(id: string): Promise<ITodo> {
  return new Todo().retrieveById(id);
}

export async function removeTodo(id: string): Promise<void> {
  new Todo().delete(id);
}

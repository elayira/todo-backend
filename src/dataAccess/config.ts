import * as dotenv from 'dotenv';
dotenv.config({path: '../../.env'})
import {config} from '../settings'
import { entities } from './entities/';

export default {
  type: 'postgres' as "postgres",
  useUTC: true,
  ...config.DATABASE,
  entities,
  migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
  synchronize: false,
}


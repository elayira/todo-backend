import { DatabaseError } from "pg";
import { logger } from "../logger";
import { IUser } from "../user/types";
import rdb from "./connections";
import { EntityConstrainError, UnknownEntityError } from "./errors";
import { User as UserEntity } from "./entities/user.entity";

export default class UserDAO implements Partial<IUser> {
  private primarydb = rdb.getRepository(UserEntity)
  id!: string;
  createdAt!: Date;
  lastName: string;
  otherNames: string;
  email: string;

  constructor({
    email,
    lastName='',
    otherNames='',
  }: {
    email: string,
    lastName?: string;
    otherNames?: string;
  }) {
    this.email = email;
    this.lastName = lastName;
    this.otherNames = otherNames
  }

  async retrieveByID(id: string): Promise<IUser> {
    let user!: IUser | null
    try {
      user = await this.primarydb.findOneBy({id})
    } catch (error) {
      throw new UnknownEntityError({message: `Retrival failed. Invalid User ID`, context: {id}})
    }
    if(!user) {
      throw new UnknownEntityError({message: `Retrival failed. Unknown User`, context: {id}})
    }
    return user
  }

  static async create({
    email,
    lastName,
    otherNames,
  }: {
    email: string,
    lastName: string,
    otherNames: string,
  }) {
    const user = new UserDAO({email, lastName, otherNames})
    return user.persist()
  }

  async persist(): Promise<IUser> {
    try {
      const user = await this.primarydb.save(
          this.primarydb.create({
          email: this.email,
          lastName: this.lastName,
          otherNames: this.otherNames,
        })
      )
      this.id = user.id
      this.createdAt = user.createdAt
    } catch (error) {
      if (error instanceof DatabaseError) {
        const exception = new EntityConstrainError({
          message: `Cannot store todo. ${error.message}`,
          context: {column: error.column},
        })
        logger.error(exception)
        throw exception
      }
    }
    return this;
  }
}
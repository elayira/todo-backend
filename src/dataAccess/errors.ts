import { ErrorBase } from "../utils/errorBases";


class Base extends ErrorBase {
  context: any;
  message: string;
  id = "";
  protected domainPrefix = 'DB';

  constructor({
    message,
    context={},
  }: {
      context?: any, 
      message: string, 
    }){
    super();
    this.context = context
    this.message = message
  }

  toJSON() {
    return {
      code: this.code,
      context: this.context,
      message: this.message,
    }
  }
}

export class UnknownEntityError extends Base {
  id = "001";
}

export class StoreUnreachableError extends Base {
  id = "002";
}

export class EntityConstrainError extends Base {
  id = "003";
}

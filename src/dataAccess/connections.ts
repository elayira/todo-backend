import { DataSource } from "typeorm"
import { StoreUnreachableError } from "./errors";
import { logger } from "../logger";
import config from "./config";

const rdb = new DataSource(config)

export async function rdbHealthCheck() {
  if(!rdb.isInitialized){
    const storeError = new StoreUnreachableError({message: 'Data store initialized'});
    logger.error(storeError)
    throw storeError
  }
}

export default rdb

import {
  PrimaryGeneratedColumn, 
  CreateDateColumn 
} from "typeorm"

export abstract class BaseModel {
  @PrimaryGeneratedColumn("uuid")
  // @ts-ignore
  id: string

  @CreateDateColumn()
  // @ts-ignore
  createdAt: Date
}
import { 
  Entity,
  ManyToOne,
  Column,
} from "typeorm"
import { Todo } from "./todo.entity"
import { BaseModel } from "./base.entity"
import { TodoStatus, TaskStatus } from "../../todo/types"

@Entity()
export class Task extends BaseModel {
  @Column({enum: TodoStatus, default: TaskStatus.UNASSIGNED})
  // @ts-ignore
  status: TaskStatus

  @Column({length: 255})
  // @ts-ignore
  description: string

  @Column({nullable: true})
  // @ts-ignore
  dueDate: Date

  @Column()
  // @ts-ignore
  todoId: string

  @ManyToOne(() => Todo, todo => todo.tasks)
  // @ts-ignore
  todo: Todo
}
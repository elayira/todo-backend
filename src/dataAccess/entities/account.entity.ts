import {
  Column, 
  Entity, 
  JoinColumn, 
  OneToOne, 
} from "typeorm"
import { User } from "./user.entity"
import { BaseModel } from "./base.entity"

@Entity()
export class Account extends BaseModel {
  @Column({unique: true, length: 255})
  // @ts-ignore
  email: string

  @Column({length: 255})
  // @ts-ignore
  password: string

  @Column()
  // @ts-ignore
  userId: string

  @OneToOne(() => User, (user) => user.account, {onDelete: 'CASCADE'})
  @JoinColumn()
  // @ts-ignore
  user: User
}
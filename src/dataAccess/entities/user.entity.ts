import {
  Entity, 
  Column,
  OneToMany,
  OneToOne,
} from 'typeorm'
import { Todo } from './todo.entity'
import { Account } from './account.entity'
import { BaseModel } from './base.entity'

@Entity()
export class User extends BaseModel {
  @Column({default: '', length: 255})
  // @ts-ignore
  lastName: string

  @Column({default: '', length: 255})
  // @ts-ignore
  otherNames: string


  @OneToMany(() => Todo, todo => todo, {onDelete: 'CASCADE'} )
  // @ts-ignore
  todos: Todo[]
  
  @OneToOne(() => Account, (account) => account.user)
  // @ts-ignore
  account: Account

  get email(): string {
    return this.account.email
  }
}
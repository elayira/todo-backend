import {
  Entity, 
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm'
import { User } from './user.entity'
import { BaseModel } from './base.entity'
import { Task } from './task.entity'
import { TodoStatus } from '../../todo/types'

@Entity()
export class Todo extends BaseModel {
  @Column({nullable: true})
  // @ts-ignore
  dueDate: Date

  @Column({length: 255})
  // @ts-ignore
  name: string

  @Column({enum: TodoStatus, default: TodoStatus.ASSIGNED})
  // @ts-ignore
  status: TodoStatus

  @ManyToOne(() => User, user => user.todos)
  // @ts-ignore
  user: User

  @OneToMany(() => Task, task => task.todo, {onDelete: 'CASCADE'})
  // @ts-ignore
  tasks: Task[]

  @Column({length: 255})
  // @ts-ignore
  description: string
}

import { User } from './user.entity';
import { Todo } from './todo.entity';
import { Task } from './task.entity';
import { Account } from './account.entity';

export const entities = [
  Todo,
  User,
  Task,
  Account
]
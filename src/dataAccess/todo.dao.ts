import { ITodo, TodoStatus } from '../todo/types';
import { EntityConstrainError, UnknownEntityError } from './errors';
import rdb from './connections';
import { logger } from '../logger';
import { DatabaseError } from 'pg';
import {Todo as TodoEntity} from './entities/todo.entity'

export default class Todo implements Partial<ITodo> {
  public primarydb = rdb.getRepository(TodoEntity)
  id!: string;
  createdAt!: Date;
  status!: ITodo['status'];
  tasks = []
  name: string;
  description: string;

  toJSON() {
    return {
      name: this.name,
      tasks: this.tasks,
      description: this.description,
      status: this.status,
      createdAt: this.createdAt,
      id: this.id
    }
  }
  
  constructor(
    name?: string,
    description?: string,

  ) {
    this.name = name || "";
    this.description = description || ""
  }

  async persist(): Promise<ITodo> {
    this.validate();
    try {
      const todo = await this.primarydb.save(
        this.primarydb.create({
          name: this.name,
          description: this.description,
          status: this.status,
        })
      )
      this.id = todo.id
      this.createdAt = todo.createdAt
    } catch (error) {
      logger.info(error)
      if (error instanceof DatabaseError) {
        const exception = new EntityConstrainError({
          message: `Cannot store todo. ${error.message}`,
          context: {column: error.column},
        })
        logger.error(exception)
        throw exception
      }
    }
    return this
  }

  protected validate(): boolean {
    if ( !Boolean(
      this.name !== undefined && 
      this.name !== "" && 
      this.description !== undefined &&
      this.description !== ""
    )) {
      throw new EntityConstrainError({
        message: 'Invalid attribute', 
        context: {name: this.name, description: this.description}
      });
    }
    return true
  }

  static async create({name, description,}: {name: string, description: string,}): Promise<ITodo> {
    const todo = new Todo(name, description);
    todo.status = TodoStatus.ASSIGNED;
    return todo.persist()
  }

  async retrieveById(id: string): Promise<ITodo> {
    let todo!: ITodo | null
    try {
      todo = await this.primarydb.findOneBy({id})
    } catch (error) {
      throw new UnknownEntityError({message: `Retrival failed. Invalid todo ID`, context: {id}})
    }
    if(!todo) {
      throw new UnknownEntityError({message: `Retrival failed. Unknown todo`, context: {id}})
    }
    return todo
  }

  async all(limit=10, skip=0): Promise<ITodo[]> {
    return this.primarydb.find({skip, take: limit})
  }

  async delete(id: string): Promise<void> {
    try {
      await this.primarydb.softDelete({id})
    } catch (error) {
      throw new UnknownEntityError({message: `Invalid todo ID`, context: {id}})
    }
  }
}

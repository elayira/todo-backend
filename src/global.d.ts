namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: NODE_ENV;
    LOG_LEVEL: "fatal" | "error" | "warn" | "info" | "debug" | "trace";
    REST_API_ROOT: string;
    GRAPHQL_ROOT: string;
    HOST: string;
    PORT: string;
    DATABASE_URL: string;
    APP_BACKEND: "graph" | 'rest'
  }
}
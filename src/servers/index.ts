export {default as restApp} from "./rest"
export {default as graphqlApp} from "./graphql"

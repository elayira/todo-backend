import { createTodo, retrieveAllTodo, retrieveTodo, removeTodo } from "../../todo";
import { RenderTodo } from "./todo.dto";
import { ControllerReturn, HttpCode, IRequest } from "./types";

export async function createTodoHandler(req: IRequest): Promise<ControllerReturn> {
  const { name, description } = req.body;
  const todo = await createTodo(name, description)
  return {
    data: new RenderTodo(todo),
    status: HttpCode.CREATED,
  }
}

export async function getAllTodoHandler(_req: IRequest): Promise<ControllerReturn> {
  const todos = await retrieveAllTodo()
  return {
    data: todos.map(todo => new RenderTodo(todo)),
    status: HttpCode.OK,
  }

}

export async function getTodoHandler(req: IRequest): Promise<ControllerReturn> {
  const todo = await retrieveTodo(req.params.id)
  return {
    data: new RenderTodo(todo),
    status: HttpCode.OK,
  }
}

export async function removeTodoHandler(req: IRequest): Promise<ControllerReturn> {
  await removeTodo(req.params.id)
  return {
    data: null,
    status: HttpCode.NO_CONTENT,
  }
}

import { ErrorBase } from "../../utils/errorBases";
import { HTTPError, HttpCode, ValidationErrorItem } from "./types";

export class BaseHttpError extends ErrorBase implements HTTPError {
  context = {};
  message: string;
  id = "";
  protected domainPrefix = '';

  constructor({
    message,
    errors
  }: {
      message: string,
      errors: ValidationErrorItem[]
    }){
    super();
    errors = errors.map( error => {
      delete error['code']
      return error
    })
    this.errors = errors
    this.message = message
  }
  errors: ValidationErrorItem[];

  public get status(){
    return parseInt(this.code, 10)
  }

  toJSON(){
    return {message: this.message}
  }
}

export class BadRequestError extends BaseHttpError{
  id = `${HttpCode.BAD_REQUEST}`
}

export class ResourceNotFoundError extends BaseHttpError{
  id = `${HttpCode.NOT_FOUND}`
}

export class InternalServerError extends BaseHttpError{
  id = `${HttpCode.INTERNAL_SERVER_ERROR}`
}


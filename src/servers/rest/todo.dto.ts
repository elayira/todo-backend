import { ITodo, TodoStatus } from "../../todo/types";
import { CreateTodoRes } from "./types";

export class RenderTodo implements CreateTodoRes {
  status: CreateTodoRes["status"];
  createdAt: CreateTodoRes["createdAt"];
  dueDate: CreateTodoRes["dueDate"];
  id: CreateTodoRes["id"];
  name: CreateTodoRes["name"];
  description: CreateTodoRes["description"];
  
  constructor(todo: ITodo) {
    this.createdAt = todo.createdAt;
    this.description = todo.description;
    this.id = todo.id;
    this.name = todo.name;
    this.dueDate = todo.dueDate || null
    this.status = TodoStatus[todo.status].toLowerCase() as CreateTodoRes["status"];
  }
}
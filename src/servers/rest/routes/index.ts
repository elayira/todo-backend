import todoRouter from './router.todo'

const routes: any = [
  todoRouter,
]

export default routes;

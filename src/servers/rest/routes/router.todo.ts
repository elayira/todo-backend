import { 
  createTodoHandler, 
  getAllTodoHandler, 
  getTodoHandler,
  removeTodoHandler,
} from "../controller.todo";
import { validateInputs } from "../middleware";
import { makeRouteHandler } from "../utility";

export default function todoRouter(router: any) {
  const route = router()
  route.use(validateInputs);
  route.post('/todos/', makeRouteHandler(createTodoHandler))
  route.get('/todos/', makeRouteHandler(getAllTodoHandler))
  route.get('/todos/:id', makeRouteHandler(getTodoHandler))
  route.delete('/todos/:id', makeRouteHandler(removeTodoHandler))
  return route
}
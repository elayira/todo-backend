import routes from "./routes";
import express, {Express} from 'express';
import { 
  HTTPLogger, 
  authenticate, 
  errorHandler, 
  healthCheck, 
  noRequestHandler 
} from './middleware';

export default function restApp() {

  const app: Express = express();
  app.use(express.json());
  app.use(HTTPLogger)
  app.use('/health', healthCheck)
  app.use(authenticate)
  routes.forEach(( route: any ) => app.use('/api/v1', route(express.Router)))
  app.use(errorHandler)
  app.use(noRequestHandler)

  return app
}

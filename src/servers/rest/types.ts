import { TodoStatus } from "../../todo/types";

type APITodoStatus = Lowercase<keyof typeof TodoStatus>
type controllerData = CreateTodoRes | CreateTodoRes[] | null;

export interface CreateTodoReqBody {
  name: string;
  description: string
}

export interface CreateTodoRes extends CreateTodoReqBody{
  readonly createdAt: Date;
  status: APITodoStatus;
  dueDate: Date | null;
  readonly id: string
}

export interface ControllerReturn {
  status: HttpCode;
  data: controllerData
}

export interface IRequest {
  body: Record<string, any>;
  originalUrl: string;
  method: 'POST' | 'GET' | 'PATCH' | 'PUT';
  params: Record<string, any>
}

export interface IResponse {
  status(httpCode: HttpCode): IResponse;
  json(payload: controllerData): IResponse
  sendStatus(httpCode: HttpCode): IResponse;
}


export type IController = (req: IRequest) => Promise<ControllerReturn>

export enum HttpCode {
  OK = 200,
  CREATED = 201,
  NO_CONTENT = 204,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  NOT_FOUND = 404,
  INTERNAL_SERVER_ERROR = 500,
  FORBIDDEN = 403
}

export interface HTTPError {
  message: string;
  status: number;
  errors: ValidationErrorItem[];
  toJSON: () => {message: string}
}
export interface ValidationErrorItem {
  location: string;
  message: string;
  code?: string;
}

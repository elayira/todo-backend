import { NextFunction } from "express";
import { UnknownEntityError, StoreUnreachableError, EntityConstrainError } from "../../dataAccess/errors";
import { ResourceNotFoundError, InternalServerError } from "./errors";
import { HttpCode, IController, IRequest, IResponse } from "./types"

export function makeRouteHandler(controller: IController) {
  return requestHandler;

  async function requestHandler(req: IRequest, res: IResponse, next: NextFunction) {
    let result: Awaited<ReturnType<IController>> = {data: null, status: HttpCode.INTERNAL_SERVER_ERROR}
    try {
      result = await controller(req)
    } catch (error) {
      next(throwHTTPError(error))
      return
    }
    return (
      result.data === null? 
      res.sendStatus(result.status) : res.status(result.status).json(result.data)
    )
  }

}

function throwHTTPError(error: any) {
  switch (true) {
    case error instanceof UnknownEntityError: {
      const options = {message: error.message, errors: [error]}
      return new ResourceNotFoundError(options)
    }
    case [
      EntityConstrainError,
      StoreUnreachableError,
    ].some(Exception => error instanceof Exception): {
      const options = {message: "Something went wrong", errors: []}
      return new InternalServerError(options)
    }
    default: {
      const options = {message: "Something went wrong", errors: []}
      return  new InternalServerError(options);
    }
  }
}

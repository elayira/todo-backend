import * as OpenApiValidator from "express-openapi-validator";
import jwt from "jsonwebtoken";
import { NextFunction, Request, Response } from "express";
import pinoHTTP from 'pino-http'
import { HTTPError, HttpCode } from "./types";
import { logger } from "../../logger";
import { config } from "../../settings";
import { rdbHealthCheck } from "../../dataAccess/connections";
import { StoreUnreachableError } from "../../dataAccess/errors";
import { InternalServerError } from "./errors";
import JwksRsa from "jwks-rsa";

export const validateInputs = OpenApiValidator.middleware({
  apiSpec: config.OPEN_API.API_SPEC,
  validateRequests: config.OPEN_API.VALIDATE_REQUESTS,
  validateResponses: config.OPEN_API.VALIDATE_RESPONSES,
});

export const HTTPLogger = pinoHTTP({logger})

export function errorHandler(
  error: HTTPError,
  _req: Request,
  response: Response,
  _next: NextFunction
): void {
  const status = error.status ?? 500;
  response.status(status).json({
    message: error.message,
    errors: error.errors,
  });
}

export function noRequestHandler(
  _req: Request,
  response: Response,
  ) {
  
    response.status(HttpCode.NOT_FOUND).json({
      message: "Resource not found",
      errors: [],
    })
}

export async function healthCheck(
  _request: Request,
  response: Response,
  next: NextFunction
) {
  try {
    await rdbHealthCheck()
    response.sendStatus(HttpCode.NO_CONTENT)
  } catch (error) {
    if (error instanceof StoreUnreachableError) {
      next(new InternalServerError({message: 'Data store health check failed', errors: []}))
    }
  }
}

export async function authenticate(
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<unknown> {
  try {
    const encodedToken = req.headers.authorization?.replace("Bearer ", "") || "";
    if (!encodedToken) {
      return res.sendStatus(HttpCode.UNAUTHORIZED);
    }
    const decodedToken = jwt.decode(encodedToken, { complete: true });
    if (!decodedToken) {
      return res.sendStatus(HttpCode.FORBIDDEN);
    }
    if (config.AUTHENTICATION.enabled) {
      try {
        const jwksClient = JwksRsa({
          jwksUri: config.AUTHENTICATION.jwksUrl,
        });
        const signingKey = await jwksClient.getSigningKey(decodedToken.header.kid);
        jwt.verify(encodedToken, signingKey.getPublicKey(), {
          algorithms: ["RS256"],
        });
      } catch {
        return res.sendStatus(HttpCode.FORBIDDEN);
      }
    }
    res.locals.token = decodedToken.payload;
    next();
  } catch (error) {
    next(error);
  }
}

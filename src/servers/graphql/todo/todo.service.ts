import { 
  Injectable 
} from '@nestjs/common';
import { 
  createTodo, 
  retrieveAllTodo, 
  retrieveTodo 
} from '../../../todo';
import { 
  CreateTodoInput 
} from './dto/todo.dto';

@Injectable()
export class TodoService {
  async retrieveAllTodo() {
    return retrieveAllTodo()
  }
  async retrieveTodo(id: string) {
    const todo = await retrieveTodo(id)
    return todo
  }

  async createTodo(args: CreateTodoInput) {
    return createTodo(args.name, args.description)
  }
}
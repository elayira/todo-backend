import { 
  ArgsType, 
  Field, 
  Int, 
  ID, 
  InputType 
} from '@nestjs/graphql';
import {
  Max, 
  Min, 
  IsUUID, 
} from 'class-validator';

@ArgsType()
export class TodosArgs {
  @Field(() => Int)
  @Min(0)
  skip = 0;

  @Field(() => Int)
  @Min(1)
  @Max(50)
  take = 25;
}

@ArgsType()
export class TodoArgs {
  @Field(() => ID)
  @IsUUID()
  // @ts-ignore
  id: string
}

@InputType()
export class CreateTodoInput {
  @Field()
  // @ts-ignore
  name: string

  @Field()
  // @Max(255)
  // @ts-ignore
  description: string;
}

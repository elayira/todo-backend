import { Resolver, Query, Args, Mutation } from "@nestjs/graphql";
import { Todo } from "./models/todo.models";
import { TodoService } from "./todo.service";
import { CreateTodoInput } from "./dto/todo.dto";

@Resolver(() => Todo)
export class TodoResolver {
  constructor(private readonly todoService: TodoService) {}

  @Query(() => Todo)
  async todo(@Args('id') id: string): Promise<Todo> {
    return this.todoService.retrieveTodo(id)
  }

  @Query(() => [Todo])
  async todos(): Promise<Todo[]> {
    // TODO: Add pagination
    return this.todoService.retrieveAllTodo()
  }

  @Mutation(() => Todo)
  async createTodo(
    @Args('payload')
    payload: CreateTodoInput
  ): Promise<Todo> {
    return this.todoService.createTodo(payload)
  }
}
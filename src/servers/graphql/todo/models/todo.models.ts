import {
  Field,
  GraphQLISODateTime,
  ID,
  ObjectType,
} from '@nestjs/graphql';
import { ITodo } from '../../../../todo/types';

@ObjectType()
export class Todo {
  @Field(() => ID)
  // @ts-ignore
  id: string

  @Field(() => GraphQLISODateTime)
  // @ts-ignore
  createdAt: Date

  @Field(() => GraphQLISODateTime, {nullable: true})
  dueDate?: Date | null

  @Field(() => String)
  // @ts-ignore
  description: string

  @Field(() => String)
  // @ts-ignore
  name: string

  @Field(() => String)
  // @ts-ignore
  status: ITodo['status']
}

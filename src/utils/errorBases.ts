export abstract class ErrorBase extends Error {
  protected abstract domainPrefix: string;
  location: string = __filename;
  
  abstract context: any;
  abstract message: string;
  abstract id: string | number;

  get code () {
    return `${this.domainPrefix}${this.id}`
  }
  get name() {
    return this.constructor.name
  }
}
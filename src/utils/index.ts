export const getObjectKeyFromValue = (obj: Record<string, string>, value: string) => {
  const keyIndex = Object.values(obj).indexOf(value)

  return Object.keys(obj)[keyIndex]
}
# Define variables
PACKAGE_NAME := $(shell node -p "require('./package.json').name")
BUILD_DIR := ./dist
ASSETA_DIR := ./assets

# Define targets
.PHONY: build clean lint start wipe graphql makemigration stop migratedb

all: clean build start

build:
	@echo "Building $(PACKAGE_NAME) API..."
	rm -fr $(BUILD_DIR)
	mkdir -p $(BUILD_DIR)
	yarn install
	yarn run build
	cp -r $(ASSETA_DIR) $(BUILD_DIR)

lint:
	@echo "Building $(PACKAGE_NAME)..."
	yarn run lint

graphql:
	@echo "generating GraphQL schema..."
	yarn run nexus:generate

makemigration:
	docker compose up -d database
	sleep 5
	yarn run migration:make

stop:
	docker compose down

migratedb:
	@echo "running migrations..."
	docker compose up -d
	sleep 5
	yarn migration:run

start: migratedb
	@echo "Starting $(PACKAGE_NAME)..."
	yarn run start

wipe:
	docker compose down
	docker volume rm -f  typescript-node-todo_database-data

clean: wipe
	@echo "Cleaning $(PACKAGE_NAME)..."
	rm -fr $(BUILD_DIR)	
